# coding=utf-8
from plateau import plateau
from joueur import joueur
from mur import Mur
from IA.MinMax import MinMax


class partie():
    def __init__(self):
        self.plateau = plateau()
        self.j1 = MinMax('E9', 'j1', None, 2, self.plateau)
        self.j0 = MinMax('E1', 'j0', self.j1, 2, self.plateau)
        self.j1.autreJoueur = self.j0
        #self.j0 = joueur('E1', 'j0')
        #self.j1 = MinMax('E9', 'j1', self.j0, 2, self.plateau)

        #self.j1 = joueur('E9', 'j1')
        self.currentJoueur = self.j0
        self.otherJoueur = self.j1

    def start(self):
        """
        déroulement de la partie
        """

        self.plateau.poserMur(Mur('A8', 'B8', 'h'))
        self.plateau.poserMur(Mur('C8', 'D8', 'h'))
        self.plateau.poserMur(Mur('E8', 'F8', 'h'))
        self.plateau.poserMur(Mur('I8', 'I9', 'v'))

        # stocker la liste des deplacement du joueur en cour ou la liste des murs qu'il peut poser
        self.plateau.initIG(self.j0.position, self.j1.position)
        while not self.aGagne():
            self.plateau.afficherIG(self.j0.position, self.j1.position)
            print(self.plateau.afficher(self.j0, self.j1))
            #self.plateau.afficherMatrice()
            print("Joueur " + str(self.currentJoueur) + " poser un mur ou deplacer: ecrire m ou d")
            reponse = self.currentJoueur.choisirAction()
            # Partie déplacement
            if reponse == 'd':
                deplacements = self.plateau.listeDeplacements(self.currentJoueur, self.otherJoueur)
                dPrint = ""
                for d in deplacements:
                    dPrint += str(d) + " "
                print (dPrint)
                reponse = self.currentJoueur.choisirDeplacement(deplacements)
                self.currentJoueur.deplacer(reponse)


            # Partie Mur
            elif reponse == 'm':
                murs = self.plateau.listeMurs(self.j0, self.j1)
                print("Quel position 1 de mur ma couille ?  ")
                m = self.currentJoueur.choisirMur(murs)
                self.plateau.poserMur(m)
            self.joueurSuivant()


    def joueurSuivant(self):
        """
        Retourne le prochain joueur qui doit joueur
        """
        if self.currentJoueur == self.j0:
            self.currentJoueur = self.j1
            self.otherJoueur = self.j0
        else:
            self.currentJoueur = self.j0
            self.otherJoueur = self.j1

    def aGagne(self):
        """
        Retourne si un joueur a gagné
        :return: booléen
        """
        if self.j0.position[1] == 9 or self.j0.position[1] == 1:
            return True
        return False

