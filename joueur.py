# coding=utf-8
from mur import Mur

class joueur():

    def __init__(self, position, nom):
        self.position = position
        self.nom = nom
        self.nbMur = 10

    def deplacer(self, position):
        """
        déplace le joueur à la position
        :param position: position à atteindre
        """
        self.position = position

    def choisirAction(self):
        a = ""
        while a != 'd' and a != 'm':
            a = input("Deplacer d ou o Mur m ?")
        return a

    def choisirDeplacement(self, d):
        reponse = input("ou voulez vous aller ")
        while reponse not in d:
            reponse = input("ou voulez vous aller ")
        return reponse

    def choisirMur(self, murs):
        m = Mur(None, None, None)
        while m not in murs:
            p1 = input("donner la position 1")
            p2 = input("donner la position 2")
            if p1[1] == p2[1]:
                o = 'h'
            else:
                o = 'v'
            m = Mur(p1, p2, o)
        return m


    def __str__(self):
        return self.nom
