# coding=utf-8
from joueur import joueur
from gameboard import gameboard
from mur import Mur
import time
import tkinter as tk


class plateau():

    def __init__(self):
        root = tk.Tk()
        self.board = gameboard(root)
        self.graphe = {
            'A1': ['A2', 'B1', None, None],
            'A2': ['A3', 'B2', 'A1', None],
            'A3': ['A4', 'B3', 'A2', None],
            'A4': ['A5', 'B4', 'A3', None],
            'A5': ['A6', 'B5', 'A4', None],
            'A6': ['A7', 'B6', 'A5', None],
            'A7': ['A8', 'B7', 'A6', None],
            'A8': ['A9', 'B8', 'A7', None],
            'A9': [None, 'B9', 'A8', None],

            'B1': ['B2', 'C1', None, 'A1'],
            'B2': ['B3', 'C2', 'B1', 'A2'],
            'B3': ['B4', 'C3', 'B2', 'A3'],
            'B4': ['B5', 'C4', 'B3', 'A4'],
            'B5': ['B6', 'C5', 'B4', 'A5'],
            'B6': ['B7', 'C6', 'B5', 'A6'],
            'B7': ['B8', 'C7', 'B6', 'A7'],
            'B8': ['B9', 'C8', 'B7', 'A8'],
            'B9': [None, 'C9', 'B8', 'A9'],

            'C1': ['C2', 'D1', None, 'B1'],
            'C2': ['C3', 'D2', 'C1', 'B2'],
            'C3': ['C4', 'D3', 'C2', 'B3'],
            'C4': ['C5', 'D4', 'C3', 'B4'],
            'C5': ['C6', 'D5', 'C4', 'B5'],
            'C6': ['C7', 'D6', 'C5', 'B6'],
            'C7': ['C8', 'D7', 'C6', 'B7'],
            'C8': ['C9', 'D8', 'C7', 'B8'],
            'C9': [None, 'D9', 'C8', 'B9'],

            'D1': ['D2', 'E1', None, 'C1'],
            'D2': ['D3', 'E2', 'D1', 'C2'],
            'D3': ['D4', 'E3', 'D2', 'C3'],
            'D4': ['D5', 'E4', 'D3', 'C4'],
            'D5': ['D6', 'E5', 'D4', 'C5'],
            'D6': ['D7', 'E6', 'D5', 'C6'],
            'D7': ['D8', 'E7', 'D6', 'C7'],
            'D8': ['D9', 'E8', 'D7', 'C8'],
            'D9': [None, 'E9', 'D8', 'C9'],

            'E1': ['E2', 'F1', None, 'D1'],
            'E2': ['E3', 'F2', 'E1', 'D2'],
            'E3': ['E4', 'F3', 'E2', 'D3'],
            'E4': ['E5', 'F4', 'E3', 'D4'],
            'E5': ['E6', 'F5', 'E4', 'D5'],
            'E6': ['E7', 'F6', 'E5', 'D6'],
            'E7': ['E8', 'F7', 'E6', 'D7'],
            'E8': ['E9', 'F8', 'E7', 'D8'],
            'E9': [None, 'F9', 'E8', 'D9'],

            'F1': ['F2', 'G1', None, 'E1'],
            'F2': ['F3', 'G2', 'F1', 'E2'],
            'F3': ['F4', 'G3', 'F2', 'E3'],
            'F4': ['F5', 'G4', 'F3', 'E4'],
            'F5': ['F6', 'G5', 'F4', 'E5'],
            'F6': ['F7', 'G6', 'F5', 'E6'],
            'F7': ['F8', 'G7', 'F6', 'E7'],
            'F8': ['F9', 'G8', 'F7', 'E8'],
            'F9': [None, 'G9', 'F8', 'E9'],

            'G1': ['G2', 'H1', None, 'F1'],
            'G2': ['G3', 'H2', 'G1', 'F2'],
            'G3': ['G4', 'H3', 'G2', 'F3'],
            'G4': ['G5', 'H4', 'G3', 'F4'],
            'G5': ['G6', 'H5', 'G4', 'F5'],
            'G6': ['G7', 'H6', 'G5', 'F6'],
            'G7': ['G8', 'H7', 'G6', 'F7'],
            'G8': ['G9', 'H8', 'G7', 'F8'],
            'G9': [None, 'H9', 'G8', 'F9'],

            'H1': ['H2', 'I1', None, 'G1'],
            'H2': ['H3', 'I2', 'H1', 'G2'],
            'H3': ['H4', 'I3', 'H2', 'G3'],
            'H4': ['H5', 'I4', 'H3', 'G4'],
            'H5': ['H6', 'I5', 'H4', 'G5'],
            'H6': ['H7', 'I6', 'H5', 'G6'],
            'H7': ['H8', 'I7', 'H6', 'G7'],
            'H8': ['H9', 'I8', 'H7', 'G8'],
            'H9': [None, 'I9', 'H8', 'G9'],

            'I1': ['I2', None, None, 'H1'],
            'I2': ['I3', None, 'I1', 'H2'],
            'I3': ['I4', None, 'I2', 'H3'],
            'I4': ['I5', None, 'I3', 'H4'],
            'I5': ['I6', None, 'I4', 'H5'],
            'I6': ['I7', None, 'I5', 'H6'],
            'I7': ['I8', None, 'I6', 'H7'],
            'I8': ['I9', None, 'I7', 'H8'],
            'I9': [None, None, 'I8', 'H9'],
        }
    def afficherMatrice(self):
        res = ""
        for i in self.graphe:
            print(i,self.graphe[i])
    def afficher(self, j1, j2):

        res = ''
        """
        function équivalent à toString en java donc affiche le plateau quand on fait print(plateau)
        *l*l*l*l*l*l*l*
        ---
        * * * * * * * *

        :param j: joueurs à afficher
        :rtype: repésentation du plateau en ligne de commande
        """
        for i in range(1, 10):
            if i == 1:
                res += '   '
                for ii in range(0, 9):
                    res += ((str(chr(ii + 65))) + '     ')
                res += '\n'
            res += (str(10 - i) + ' ')
            for ii in range(0, 9):
                case = (chr(ii + 65) + str(10 - i))
                if case != j1.position and case != j2.position:
                    res += ' * '
                if case == j1.position:
                    res += ' O '
                if case == j2.position:
                    res += ' X '
                if self.graphe[case][1] is None:
                    res += ' | '
                if self.graphe[case][1] is not None:
                    res += '   '
            res += (str(10 - i) + ' ')
            res += '\n '
            res += '  '
            for ii in range(0, 9):
                case = (chr(ii + 65) + str(10 - i))
                if self.graphe[case][2] is not None:
                    res += '      '
                else:
                    res += '----- '
            res += '\n'
            if i == 9:
                res += '   '
                for ii in range(0, 9):
                    res += ((str(chr(ii + 65))) + '     ')
                res += '\n'
        return res

    # en entrée :direction 0 1 2 3  haut droite bas gauche
    def enleverLien2cases(self, case1, direction1, case2, direction2):
        """
        Enleve le lien entre deux sommets du graphe
        :param case1: premiere case à déconnecter par exemple 'A1'
        :param direction1: direction de la seconde case par rapport à la premiere 0:HAUT 1:DROITE 2:GAUCHE 3:BAS
        :param case2: deuxième case à déconnecter par exemple 'B1'
        :param direction2: direction de la première case par rapport à la seconde 0:HAUT 1:DROITE 2:GAUCHE 3:BAS
        """


        self.graphe[case1][direction1] = None
        self.graphe[case2][direction2] = None

    def ajouterLien2cases(self, case1, direction1, case2, direction2):
        """
        Crée un lien entre deux sommets du graphe
        :param case1: premiere case à connecter par exemple 'A1'
        :param direction1: direction de la seconde case par rapport à la premiere 0:HAUT 1:DROITE 2:GAUCHE 3:BAS
        :param case2: deuxième case à connecter par exemple 'B1'
        :param direction2: direction de la première case par rapport à la seconde 0:HAUT 1:DROITE 2:GAUCHE 3:BAS
        """
        self.graphe[case1][direction1] = case2
        self.graphe[case2][direction2] = case1

    # liste des deplacements valides par un joueur
    def listeDeplacements(self, j1, j2):
        """
        Renvoie la liste des déplacements possible pour un joueur donné
        On donne les deux joueurs au cas ou un saut est possible
        :param j1: joueur qui joue
        :param j2: autre joueur

        :return: liste des cases ou le joueur peut bouger
        """
        posJ = j1.position
        res = []
        # for p in self.graphe[posJ]:
        for p in range(4):
            dp = self.graphe[posJ][p]

            if dp == j2.position:
                if self.graphe[dp][p] is not None:
                    res.append(self.graphe[dp][p])
                else:
                    if self.graphe[dp][(p - 1) % 4] is not None:
                        res.append(self.graphe[dp][(p - 1) % 4])
                    if self.graphe[dp][(p + 1) % 4] is not None:
                        res.append(self.graphe[dp][(p + 1) % 4])
            elif dp is not None:
                res.append(dp)

        return res

    def listeMurs(self, j1, j2):
        """
        Renvoie la liste des murs que l'on peut poser sans enlever la connexité
        :param j1: joeur1
        :param j2:joueur2
        :return: liste des murs
        """
        res = []
        m = None
        for case in self.graphe:
            mh = self.positionValideMur(case, 'h', j1, j2)
            mv = self.positionValideMur(case, 'v', j1, j2)
            if mh:
                res.append(mh)
            if mv:
                res.append(mv)
        return res

    def positionValideMur(self, position, orientation, j1, j2):
        """
        Teste si on peut poser un mur depuis la position
        c'est à dire si il n'y a pas deja de mur et si le mur ne croise pas un autre
        :param j1: joeur1
        :param j2: joueur2
        :param position: position de départ du mur
        :param orientation: orientation
        :return retourne le mur si il est possible de le poser ou false si c'est pas possible
        """
        ok = False
        if orientation == 'h':
            p1 = self.graphe[position][0]
            if p1 is not None:
                p2 = self.graphe[p1][1]
                if p2 is not None:
                    p3 = self.graphe[p2][2]
                    if p3 is not None:
                        self.enleverLien2cases(position, 0, p1, 2)
                        self.enleverLien2cases(p3, 0, p2, 2)
                        if self.plateauValide(j1, j2):
                            self.ajouterLien2cases(position, 0, p1, 2)
                            self.ajouterLien2cases(p3, 0, p2, 2)
                            return Mur(position, p3, orientation)
                        self.ajouterLien2cases(position, 0, p1, 2)
                        self.ajouterLien2cases(p3, 0, p2, 2)
        elif orientation == 'v':
            p1 = self.graphe[position][3]
            if p1 is not None:
                p2 = self.graphe[p1][0]
                if p2 is not None:
                    p3 = self.graphe[p2][1]
                    if p3 is not None:
                        self.enleverLien2cases(position, 3, p1, 1)
                        self.enleverLien2cases(p3, 3, p2, 1)
                        if self.plateauValide(j1, j2):
                            self.ajouterLien2cases(position, 3, p1, 1)
                            self.ajouterLien2cases(p3, 3, p2, 1)
                            return Mur(position, p3, orientation)
                        self.ajouterLien2cases(position, 3, p1, 1)
                        self.ajouterLien2cases(p3, 3, p2, 1)
        return False

    def plateauValide(self, j1, j2):
        """
        Teste si le plateau est valide (si les deux joueurs peuvent gagner
        :param j1: joueur 1
        :param j2: joueur 2
        :return: booléen si le plateau est valide
        """
        arriveeJ1 = ["A9", "B9", "C9", "D9", "E9", "F9", "G9", "H9", "I9"]
        arriveeJ2 = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1"]
        if j1.nom == 'j0' :
            return (self.distance([j1.position], arriveeJ1) is not None) & \
               (self.distance([j2.position], arriveeJ2) is not None)
        else:
            return (self.distance([j1.position], arriveeJ2) is not None) & \
                   (self.distance([j2.position], arriveeJ1) is not None)

    def poserMur(self, mur):
        """
        Pose un mur
        :param mur: le mur à poser
        """
        if mur.orientation == 'v':
            self.enleverLien2cases(mur.position1, 3, self.graphe[mur.position1][3], 1)
            self.enleverLien2cases(mur.position2, 3, self.graphe[mur.position2][3], 1)
        else:
            self.enleverLien2cases(mur.position1, 0, self.graphe[mur.position1][0], 2)
            self.enleverLien2cases(mur.position2, 0, self.graphe[mur.position2][0], 2)

    def enleverMur(self, mur):
        """
        enleve un mur
        :param mur: le mur à enlever
        """
        if mur.orientation == 'v':
            self.ajouterLien2cases(mur.position1, 3, self.graphe[mur.position1][3], 1)
            self.ajouterLien2cases(mur.position2, 3, self.graphe[mur.position2][3], 1)
        else:
            self.ajouterLien2cases(mur.position1, 0, self.graphe[mur.position1][0], 2)
            self.ajouterLien2cases(mur.position2, 0, self.graphe[mur.position2][0], 2)

    def distanceToWin(self, joueur):
        cible = ["A9", "B9", "C9", "D9", "E9", "F9", "G9", "H9", "I9"]
        cible2 = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1"]

        if joueur.nom == 'j0':
            return self.distance([joueur.position], cible)
        else:
            return self.distance([joueur.position], cible2)


    def distance(self, listeDepart, listeArrivee):
        """
        Distance entre deux listes de sommets du graphe
        :param listeDepart: liste de sommet de départ
        :param listeArrivee: liste de sommet d'arrivée
        :return: distance minimale séparant les deux listes de sommets
        """
        start_time = time.time()
        compteur = 0
        fait = []
        while listeDepart:
            temp = []
            for s in listeDepart:
                if s is not None:
                    temp = Union(temp, self.graphe[s])
                    for i in listeArrivee:
                        if (str(s) == i):
                            #print("--- %s seconds ---" % (time.time() - start_time))
                            return (compteur)
            removeL1inL2(fait, temp)
            removeL1inL2(listeDepart, temp)
            fait = Union(fait, listeDepart)
            listeDepart = temp
            compteur += 1

    def afficherIG(self, J1, J2):


        # board.addmur('mur34',imgMurv)
        self.board.actualiser(J1, J2, self.graphe)


    def initIG(self, J1, J2):
        self.board.pack(side="top", fill="both", expand="true", padx=4, pady=4)
        gameisruning = True
        self.board.addpiece("J1", self.board.imgJ1, 0, 4)
        self.board.addpiece("J2", self.board.imgJ2, 8, 4)
        self.board.actualiser(J1, J2, self.graphe)


def removeL1inL2(list_1, list_2):
    """
    Enleve tout les éléments de L1 dans L2
    :param list_1: liste 1 L1
    :param list_2: liste 2 L2
    """
    for element in list_1:
        if element in list_2:
            list_2.remove(element)


def Union(list1, list2):
    """
     retourne l'union des deux listes en paramêtre
    :param list1:
    :param list2:
    :return:
    """
    result = list(set(list1 + list2))
    return result

