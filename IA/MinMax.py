import time

from plateau import plateau
from mur import Mur
from joueur import joueur
import math
import copy


class MinMax(joueur):
    def __init__(self, position, nom, autreJoueur, niveau, plateau):
        joueur.__init__(self, position, nom)
        self.autreJoueur = autreJoueur
        self.niveau = niveau
        self.plateau = plateau
        self.coup = None
        self.appel = 0

    def evaluation(self):

        distToWinJcourant = self.plateau.distanceToWin(self)
        distToWinAutreJoueur = self.plateau.distanceToWin(self.autreJoueur)

        if self.nom == 'j0' and self.position[1] == 9 or \
                self.nom == 'j1' and self.position[1] == 1:
            return 2 ** 30
        if self.autreJoueur == 'j0' and self.autreJoueur.position[1] == 9 or \
                self.autreJoueur.nom == 'j1' and self.autreJoueur.position[1] == 1:
            return -(2 ** 30)
        #print(distToWinAutreJoueur, "   ", distToWinJcourant)
        #print(self.plateau.afficher(self, self.autreJoueur))
        return distToWinAutreJoueur - distToWinJcourant

    def max(self, profondeur, minmax):
        self.appel += 1
        if profondeur == 0 or self.aGagne():
            return self.evaluation()
        elif minmax:
            # print("max")
            score = -(2 ** 30)
            for d in self.plateau.listeDeplacements(self, self.autreJoueur):
                posActu = self.position
                self.deplacer(d)
                score = max(score, self.max(profondeur - 1, not minmax))
                self.deplacer(posActu)
            for m in self.plateau.listeMurs(self, self.autreJoueur):
                case1 = m.position1
                case2 = m.position2
                if m.orientation == 'h':
                    case3 = self.plateau.graphe[m.position1][0]
                    case4 = self.plateau.graphe[m.position2][0]
                    self.plateau.enleverLien2cases(case1, 0, case3, 2)
                    self.plateau.enleverLien2cases(case2, 0, case4, 2)
                    score = max(score, self.max(profondeur - 1, not minmax))
                    self.plateau.ajouterLien2cases(case1, 0, case3, 2)
                    self.plateau.ajouterLien2cases(case2, 0, case4, 2)
                elif m.orientation == 'v':
                    case3 = self.plateau.graphe[m.position1][3]
                    case4 = self.plateau.graphe[m.position2][3]
                    self.plateau.enleverLien2cases(case1, 3, case3, 1)
                    self.plateau.enleverLien2cases(case2, 3, case4, 1)
                    #print(m)
                    score = max(score, self.max(profondeur - 1, not minmax))
                    self.plateau.ajouterLien2cases(case1, 3, case3, 1)
                    self.plateau.ajouterLien2cases(case2, 3, case4, 1)

        else:
            # print("min")
            score = 2 ** 30
            for d in self.plateau.listeDeplacements(self.autreJoueur, self):
                posActu = self.autreJoueur.position
                self.autreJoueur.deplacer(d)
                score = min(score, self.max(profondeur - 1, not minmax))

                self.autreJoueur.deplacer(posActu)

            for m in self.plateau.listeMurs(self, self.autreJoueur):
                case1 = m.position1
                case2 = m.position2
                if m.orientation == 'h':
                    case3 = self.plateau.graphe[m.position1][0]
                    case4 = self.plateau.graphe[m.position2][0]
                    self.plateau.enleverLien2cases(case1, 0, case3, 2)
                    self.plateau.enleverLien2cases(case2, 0, case4, 2)
                    score = min(score, self.max(profondeur - 1, not minmax))
                    self.plateau.ajouterLien2cases(case1, 0, case3, 2)
                    self.plateau.ajouterLien2cases(case2, 0, case4, 2)
                elif m.orientation == 'v':
                    case3 = self.plateau.graphe[m.position1][3]
                    case4 = self.plateau.graphe[m.position2][3]
                    self.plateau.enleverLien2cases(case1, 3, case3, 1)
                    self.plateau.enleverLien2cases(case2, 3, case4, 1)
                    score = min(score, self.max(profondeur - 1, not minmax))
                    self.plateau.ajouterLien2cases(case1, 3, case3, 1)
                    self.plateau.ajouterLien2cases(case2, 3, case4, 1)

        return score

    def aGagne(self):
        return (self.nom == 'j0' and self.position[1] == 9 or \
                self.nom == 'j1' and self.position[1] == 1) or \
               self.autreJoueur == 'j0' and self.autreJoueur.position[1] == 9 or \
               self.autreJoueur.nom == 'j1' and self.autreJoueur.position[1] == 1

    def choisirAction(self):
        self.appel = 0
        score = -(2 ** 30)
        for d in self.plateau.listeDeplacements(self, self.autreJoueur):
            posActu = self.position
            self.deplacer(d)
            eval = self.max(self.niveau - 1, False)
            self.deplacer(posActu)
            if eval > score:
                score = eval
                bestMove = d
            #print(eval, "   ", d)

        for m in self.plateau.listeMurs(self, self.autreJoueur):
            case1 = m.position1
            case2 = m.position2
            if m.orientation == 'h':
                case3 = self.plateau.graphe[m.position1][0]
                case4 = self.plateau.graphe[m.position2][0]
                self.plateau.enleverLien2cases(case1, 0, case3, 2)
                self.plateau.enleverLien2cases(case2, 0, case4, 2)
                eval = self.max(self.niveau - 1, False)
                self.plateau.ajouterLien2cases(case1, 0, case3, 2)
                self.plateau.ajouterLien2cases(case2, 0, case4, 2)
            elif m.orientation == 'v':
                case3 = self.plateau.graphe[m.position1][3]
                case4 = self.plateau.graphe[m.position2][3]
                self.plateau.enleverLien2cases(case1, 3, case3, 1)
                self.plateau.enleverLien2cases(case2, 3, case4, 1)
                eval = self.max(self.niveau - 1, False)
                self.plateau.ajouterLien2cases(case1, 3, case3, 1)
                self.plateau.ajouterLien2cases(case2, 3, case4, 1)

            if eval > score:
                score = eval
                bestMove = m
            #print(eval, "   ", m)

        self.coup = bestMove
        print(self.appel)
        if type(self.coup) is Mur:
            return 'm'
        else:
            return 'd'

    def choisirDeplacement(self, d):
        print("COUP : ", self.coup)
        return self.coup

    def choisirMur(self, murs):
        print("COUP :", self.coup)
        return self.coup

    def __str__(self):
        return self.nom
