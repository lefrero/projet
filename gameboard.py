import tkinter as tk

class gameboard(tk.Frame):
    def __init__(self, parent, rows=9, columns=9, size=100, color1="white", color2="white"):
        '''size is the size of a square, in pixels'''

        self.rows = rows
        self.columns = columns
        self.size = size
        self.color1 = color1
        self.color2 = color2
        self.pieces = {}
        self.imgMurh= tk.PhotoImage(file='assets/demimur.png')
        self.imgMurv= tk.PhotoImage(file='assets/demimurv.png')
        self.imgJ1 = tk.PhotoImage(file='assets/joueur1.png')
        self.imgJ2 = tk.PhotoImage(file='assets/joueur2.png')
        canvas_width = columns * size
        canvas_height = rows * size

        tk.Frame.__init__(self, parent)
        self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0,
                                width=canvas_width, height=canvas_height, background="black")
        self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)

        # this binding will cause a refresh if the user interactively
        # changes the window size
        self.canvas.bind("<Configure>", self.refresh)

    def addpiece(self, name, image, row, column):
        '''Add a piece to the playing board'''
        self.canvas.create_image(0,0, image=image, tags=(name, "piece"), anchor="c")
        self.placepiece(name, column, row)

    def addmur(self, name, image, row, column):
        '''Add a MUR EN ACIER TREMPE to the playing board'''
        self.canvas.create_image(row,column, image=image, tags=(name, "piece"), anchor="nw")
        '''Place a piece at the given row/column'''
        self.pieces[name] = (row-(1/2), column-(1/2))
        '''
        x0 = ((column * self.size) - (30 * self.size))
        y0 = ((row * self.size)- (30 * self.size))
        '''
        x0 = 0
        y0 = 0
        #self.canvas.coords(name, x0, y0)

    def placepiece(self, name, row, column):
        '''Place a piece at the given row/column'''
        self.pieces[name] = (row, column)
        x0 = ((column * self.size) + int(self.size/2))
        y0 = ((row * self.size) + int(self.size/2))
        self.canvas.coords(name, x0, y0)

    def refresh(self, event):
        '''Redraw the board, possibly in response to window being resized'''
        xsize = int((event.width-1) / self.columns)
        ysize = int((event.height-1) / self.rows)
        self.size = min(xsize, ysize)
        self.canvas.delete("square")
        color = self.color2
        for row in range(self.rows):
            color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        for name in self.pieces:
            self.placepiece(name, self.pieces[name][0], self.pieces[name][1])
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")

    def actualiser(self,J1,J2,graph):
        nb = 0
        #déplacement des pieces
        self.placepiece("J1",int(J1[1])-1,(ord(J1[0])-65))
        self.placepiece("J2",int(J2[1])-1,(ord(J2[0])-65))
        #on enleve absolument tous les murs
        for i in range (0,400):
            self.canvas.delete('mur'+str(i))

        #analyse de toutes les cases et place les murs en concéquence
        for i in graph:
            x = (ord(i[0])-65)
            y = int(i[1])-1
            if graph[i][2] is None :
                self.addmur('mur'+str(nb),self.imgMurh,y,x)
                nb+=1
            if graph[i][3] is None :
                self.addmur('mur'+str(nb),self.imgMurv,y,x)
                nb+=1
            if graph[i][0] is None :
                self.addmur('mur'+str(nb),self.imgMurh,y+1,x)
                nb+=1
            if graph[i][1] is None :
                self.addmur('mur'+str(nb),self.imgMurv,y,x+1)
                nb+=1

        self.canvas.delete("square")
        color = self.color2
        for row in range(self.rows):
            color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        for name in self.pieces:
            self.placepiece(name, self.pieces[name][0], self.pieces[name][1])
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")
