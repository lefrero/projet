class Mur:
    def __init__(self, position1, position2 , orientation):
        self.position1 = position1
        self.position2 = position2
        self.orientation = orientation

    def __str__(self):
        return self.position1+"  "+self.position2+"   "+self.orientation

    def __eq__(self, other):
        return self.position1 == other.position1 and self.position2 == other.position2 \
               and self.orientation == other.orientation


