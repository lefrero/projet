import copy
from dis import dis

class case:
        def __init__(self, j, g, h):
            self.j = 0
            self.g = 0
            self.h = 0

grille = {
    'A1': {'A2', 'B1',},
    'A2': {'A1', 'A3','B2'},
    'A3': {'A2', 'A4','B3'},
    'A4': {'A3', 'A5','B4'},
    'A5': {'A4', 'A6','B5'},
    'A6': {'A5', 'A7','B6'},
    'A7': {'A6', 'A8','B7'},
    'A8': {'A7', 'A9','B8'},
    'A9': {'A8','B9'},

    'B1': {'A1', 'B2','C1'},
    'B2': {'B1', 'B3','C2','A2'},
    'B3': {'B2', 'B4','C3','A3'},
    'B4': {'B3', 'B5','C4','A4'},
    'B5': {'B4', 'B6','C5','A5'},
    'B6': {'B5', 'B7','C6','A6'},
    'B7': {'B6', 'B8','C7','A7'},
    'B8': {'B7', 'B9','C8','A8'},
    'B9': {'B8','A9','C9'},

    'C1': {'B1', 'C2','D1'},
    'C2': {'C1', 'C3','D2','B2'},
    'C3': {'C2', 'C4','D3','B3'},
    'C4': {'C3', 'C5','D4','B4'},
    'C5': {'C4', 'C6','D5','B5'},
    'C6': {'C5', 'C7','D6','B6'},
    'C7': {'C6', 'C8','D7','B7'},
    'C8': {'C7', 'C9','D8','B8'},
    'C9': {'C8','B9','D9'},

    'D1': {'C1', 'D2','E1'},
    'D2': {'D1', 'D3','E2','C2'},
    'D3': {'D2', 'D4','E3','C3'},
    'D4': {'D3', 'D5','E4','C4'},
    'D5': {'D4', 'D6','E5','C5'},
    'D6': {'D5', 'D7','E6','C6'},
    'D7': {'D6', 'D8','E7','C7'},
    'D8': {'D7', 'D9','E8','C8'},
    'D9': {'D8','C9','E9'},

    'E1': {'D1', 'E2','F1'},
    'E2': {'E1', 'E3','F2','D2'},
    'E3': {'E2', 'E4','F3','D3'},
    'E4': {'E3', 'E5','F4','D4'},
    'E5': {'E4', 'E6','F5','D5'},
    'E6': {'E5', 'E7','F6','D6'},
    'E7': {'E6', 'E8','F7','D7'},
    'E8': {'E7', 'E9','F8','D8'},
    'E9': {'E8','D9','F9'},

    'F1': {'E1', 'F2','G1'},
    'F2': {'F1', 'F3','G2','E2'},
    'F3': {'F2', 'F4','G3','E3'},
    'F4': {'F3', 'F5','G4','E4'},
    'F5': {'F4', 'F6','G5','E5'},
    'F6': {'F5', 'F7','G6','E6'},
    'F7': {'F6', 'F8','G7','E7'},
    'F8': {'F7', 'F9','G8','E8'},
    'F9': {'F8','E9','G9'},

    'G1': {'F1', 'G2','H1'},
    'G2': {'G1', 'G3','H2','F2'},
    'G3': {'G2', 'G4','H3','F3'},
    'G4': {'G3', 'G5','H4','F4'},
    'G5': {'G4', 'G6','H5','F5'},
    'G6': {'G5', 'G7','H6','F6'},
    'G7': {'G6', 'G8','H7','F7'},
    'G8': {'G7', 'G9','H8','F8'},
    'G9': {'G8','F9','H9'},

    'H1': {'G1', 'H2','I1'},
    'H2': {'H1', 'H3','I2','G2'},
    'H3': {'H2', 'H4','I3','G3'},
    'H4': {'H3', 'H5','I4','G4'},
    'H5': {'H4', 'H6','I5','G5'},
    'H6': {'H5', 'H7','I6','G6'},
    'H7': {'H6', 'H8','I7','G7'},
    'H8': {'H7', 'H9','I8','G8'},
    'H9': {'H8','G9','I9'},
    
    'I1': {'I2', 'H1'},
    'I2': {'I1', 'I3','H2'},
    'I3': {'I2', 'I4','H3'},
    'I4': {'I3', 'I5','H4'},
    'I5': {'I4', 'I6','H5'},
    'I6': {'I5', 'I7','H6'},
    'I7': {'I6', 'I8','H7'},
    'I8': {'I7', 'I9','H8'},
    'I9': {'I8','H9'}
}

def creationPlateau(tailleP):
    tableau = [[1 for x in range(tailleP)] for y in range(tailleP)] 
    for i in range(0,tailleP):
        for j in range(0,tailleP):
            tableau[j][i] = case(0,0,0)
    return tableau

def afficherPlateau(tableau):
    for i in range(0,len(tableau)):
        print("-                            -")
        for j in range(0,len(tableau)):
            if(tableau[j][i].h == 1):
                print("--",end="")
            if(tableau[j][i].h == 0):
                print("  ",end="")
        print("\n",end="")

        for j in range(0,len(tableau)):
            if(tableau[j][i].g == 1):
                print("||",end="")
            if(tableau[j][i].g == 0):
                print("| ",end="")
            if(tableau[j][i].j == 1):
                print("x",end="")
            if(tableau[j][i].j == 0):
                print(" ",end="")
        print("|\n",end="")
    print("-                            -")

def murpossible(sens,case,graph):
    copiegraph = []
    copiegraph = copy.deepcopy(graph)
    placerMur(sens,case,copiegraph)
    if (sens == 'v'):
        if (distance(pointDepart,cible,copiegraph) == None) or (case not in listemursV):
            return False
    if (sens == 'h'):
        if (distance(pointDepart,cible,copiegraph) == None) or (case not in listemursH):
            return False

def placerMur(sens,case,graph):
        if(sens=="h"):
            caseYplusun = case[0]+(str(int(case[1])+1))
            caseXplusun = (str(chr(ord(case[0])+1)))+case[1]
            caseXYplusun = (str(chr(ord(case[0])+1)))+(str(int(case[1])+1))
            
            graph[case].remove(caseYplusun)
            graph[caseYplusun].remove(case)

            graph[caseXplusun].remove(caseXYplusun)
            graph[caseXYplusun].remove(caseXplusun)

        if(sens=="v"):
            caseYmoinsun = case[0]+(str(int(case[1])-1))
            caseXmoinsun = (str(chr(ord(case[0])-1)))+case[1]
            caseXYmoinsun = (str(chr(ord(case[0])-1)))+(str(int(case[1])-1))

            graph[case].remove(caseXmoinsun)
            graph[caseXmoinsun].remove(case)

            graph[caseYmoinsun].remove(caseXYmoinsun)
            graph[caseXYmoinsun].remove(caseYmoinsun)

def placerMurSafe(sens,case,graph):
    x = ord(case[0])-65
    y = int(case[1])
    copiegraph = graph.copy()
    if ((case not in listemursV) and (case not in listemursH)):
        return 0
    if (murpossible(sens,case,copiegraph) == False):
        return 0
    else:
        if(sens=="h"):
            #affichage
            tableau[x][y].h = 1
            tableau[x+1][y].h = 1

            caseYplusun = case[0]+(str(int(case[1])+1))
            caseXplusun = (str(chr(ord(case[0])+1)))+case[1]
            caseXYplusun = (str(chr(ord(case[0])+1)))+(str(int(case[1])+1))
            
            print('mur : ' ,case, caseYplusun,caseXplusun,caseXYplusun)
            placerMur(sens,case,graph)
            listemursH.remove(case)
        if(sens=="v"):
            #affichage
            tableau[x][y].g = 1
            tableau[x][y+1].g = 1
            
            caseYmoinsun = case[0]+(str(int(case[1])-1))
            caseXmoinsun = (str(chr(ord(case[0])-1)))+case[1]
            caseXYmoinsun = (str(chr(ord(case[0])-1)))+(str(int(case[1])-1))

            print('mur : ', case, caseXmoinsun, caseYmoinsun, caseXYmoinsun)
            placerMur(sens,case,graph)
            listemursV.remove(case)

def distance(afaire,cible,graph):
    compteur=0
    fait=set()
    while afaire:
        temp=set()
        for s in afaire:
            temp|=graph[s]
            for i in cible:
                if(str(s)==i): 
                    return(compteur)
        temp-=fait
        temp-=afaire
        fait|=afaire
        afaire=set(temp)
        compteur+=1

def deplacementsPossibles(case,graph):
    xJ = case[0]
    yJ = case[1]
    res=[]
    droite = (str(chr(ord(xJ)+1))+str(yJ))
    gauche = (str(chr(ord(xJ)-1))+str(yJ))
    haut = (str(chr(ord(xJ)))+str(int(yJ)+1))
    bas =(str(chr(ord(xJ)))+str(int(yJ)-1))

    #teste à droite
    if(xJ != 'I'):
        if(droite in graph[case]):
            res.append(droite)
    #test à gauche
    if(xJ != 'A'):
        if(gauche in graph[case]):
            res.append(gauche)
    #test en haut
    if(yJ != '9'):
        if(haut in graph[case]):
            res.append(haut)
    #test en bas
    if(yJ != 'O'):
        if(bas in graph[case]):
            res.append(bas)
    return res

def initmursH():
    res=[]
    for i in range (1,9):
        for ii in range(1,9):
            res.append(chr(i+64)+str(ii))
    return res

def initmursV():
    res=[]
    for i in range (2,10):
        for ii in range(2,10):
            res.append(chr(i+64)+str(ii))
    return res

def meilleurCoup(positionJ1,positionJ2, graph):
    '''
    tester position joueur grille
    tester les déplacements noter
    tester les murs v et h noter
    priviliégier les déplacements
    note = distance J2 - distance J1
    '''
    print("prout")
    meilleurdeplacement = ''
    meilleurdeplacementscore = 1000
    
    meilleurmurH = ''
    meilleurscoreH = 1000

    meilleurmurV = ''
    meilleurscoreV = 1000

    #deplacement
    for i in (deplacementsPossibles(str(positionJ1.copy().pop()),graph)):
        testscore = distance(positionJ2,cible2,graph) - distance({i},cible,graph)
        if testscore < meilleurdeplacementscore :
            score = copy.deepcopy(testscore)
            meilleurdeplacement = copy.deepcopy(i)

    #murH
    for i in listemursH:
        graphtest = copy.deepcopy(graph)
        placerMurSafe('h',i,graphtest)
        testscore = distance(positionJ2,cible2,graphtest) - distance(positionJ1,cible,graphtest)
        if testscore < meilleurscoreH :
            meilleurscoreH = copy.deepcopy(testscore)
            meilleurmurH = copy.deepcopy(i)
    
    #murV
    for i in listemursV:
        graphtest = copy.deepcopy(graph)
        placerMurSafe('v',i,graphtest)
        testscore = distance(positionJ2,cible2,graphtest) - distance(positionJ1,cible,graphtest)
        if testscore < meilleurscoreV :
            meilleurscoreV = copy.deepcopy(testscore)
            meilleurmurV = copy.deepcopy(i)

    if (meilleurscoreH >= meilleurdeplacementscore):
        placerMurSafe('h',str(meilleurmurH),graph)
    if(meilleurscoreV >= meilleurdeplacementscore):
        placerMurSafe('v',str(meilleurmurV),graph)
    else:
        positionJ1 = {meilleurdeplacement}

    print('jej')
    
    
    return score, meilleurdeplacement




tailleP = 9
tableau = creationPlateau(tailleP)

#pour la distance
pointDepart={"D1"}
pointDepart2={"D9"}
cible=["A9","B9","C9","D9","E9","F9","G9","H9","I9"]
cible2=["A1","B1","C1","D1","E1","F1","G1","H1","I1"]

listemursH = initmursH()
listemursV = initmursV()

tableau[4][0].j = 1

afficherPlateau(tableau)

