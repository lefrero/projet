from partie import partie
from plateau import plateau
from mur import Mur
from joueur import joueur


pl = plateau()
j1 = joueur('E1', 'j1')
j2 = joueur('E9', 'j2')

pl.enleverLien2cases('A1', 0, 'A2', 2)
pl.enleverLien2cases('B1', 0, 'B2', 2)

pl.afficherMatrice()